﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tiempo : MonoBehaviour {

	// Use this for initialization

	public Text recordText; 
	void Start () {
		recordText = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!Gover.dead) {
			Time.timeSinceLevelLoad.ToString ();
			recordText.text = "Time : " + Time.timeSinceLevelLoad;
		}
		}

}
