﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour {
	public Rigidbody2D rb;
	public float speed;
	private bool jump;
	private bool isDead = false;
	// Use this for initialization
	void Start () {
		
		rb = GetComponent <Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (isDead == false) {	
			if (Input.GetMouseButtonDown (0)) 
			{
				jump = true;
				rb.velocity = new Vector2 (0, 0);
				StartCoroutine ("stopJump");

			}	
		}
	}

	void FixedUpdate () {
		
		if (jump)
				{
			rb.AddForce (Vector3.up * speed);

		}
	}

	IEnumerator stopJump ()
	{
		yield return new WaitForSeconds(.1f);
		jump = false;

	} 
	void OnCollisionEnter2D (){
		
		isDead= true;

	flappyBirdGM.GM.birdDied();
	}

}

