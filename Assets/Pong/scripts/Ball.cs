﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour {

	private Rigidbody2D rb;
	private int Player1;
	private int Player2;
	public Text score;


	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		ApplyForce ();
		
	}
	
	// Update is called once per frame
	void ApplyForce () {

		int rnd = Random.Range (0, 100);
		if (rnd < 50) {
			rb.velocity = new Vector3 (3,Random.Range (-2,2), 0);
		} else {
			rb.velocity = new Vector3 (-3,Random.Range (-2,2), 0);
		}


	

	}

	void OnTriggerEnter2D(Collider2D other)

	{

		if (other.gameObject.CompareTag ("wall1"))
		{
			 
		Player2 ++;

		
			SetOrigin();
			Invoke ("ApplyForce", 3);

		}

		if (other.gameObject.CompareTag ("wall2"))
		{

			Player1 ++;
			SetOrigin();
			Invoke ("ApplyForce", 3);
		
	}
		score.text = Player1.ToString () + "       -       " + Player2.ToString ();

	}    
	public void SetOrigin ()
	{
		rb.velocity = new Vector2 (0f, 0f);
		this.transform.position = new Vector3 (0f, 0f, 0f);

	}


}

	
