﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moverackets : MonoBehaviour {
	public KeyCode up;
	public KeyCode down;
	float speed;
	float finalSpeed;

	// Use this for initialization
	void Start () {
		speed = 0.15f;
		
	}
	
	// Update is called once per frame


	void Update ()
	{
		//si se presiona la tecla asignada como up en unity
		if (Input.GetKey (up)) {
			//si la posicion y es mayor que 1.8 es decir supera el limite 
			if (transform.localPosition.y > 1.8f) {
				//la variable cambia a cero
				finalSpeed = 0;
			} else {
				// la variable se iguala al valor original,speed 
				finalSpeed = speed;
			
			}
			//movimiento solo en y
			transform.Translate (0, finalSpeed, 0);
		}

		if (Input.GetKey (down)) {

			if (transform.localPosition.y < -1.8f) {
				finalSpeed = 0;
			} else {
				finalSpeed = speed;

			}


			transform.Translate (0, -finalSpeed, 0);
		}

	}
}
