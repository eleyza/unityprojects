﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public float bspeed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.up*bspeed);

	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Enemy") {

			Destroy (other.gameObject);	
			Destroy (gameObject);
			Score.PlayerScore += 5;
			} 
		else if (other.tag == "Defense")
			Destroy (gameObject);
}

}
