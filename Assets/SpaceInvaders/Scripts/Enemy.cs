﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {
	
	public int Espeed;
	public float fireRate = 1;
	public GameObject EnemyToSpawn;
	public GameObject Shot;
	private Transform enemigoPadre;
	public Text winText;


	// Use this for initialization
	void Start () {
		winText.enabled = false;
		enemigoPadre = GetComponent<Transform> ();
		InvokeRepeating ("Movement", 0.1f, 0.3f);

		spawn (); 


	}
		

	void spawn () 

	{

		for (int j = 0; j < 4; j++) 
		{
			for(int i= -4 ;i< 4; i++)
			{
				GameObject clone = Instantiate(EnemyToSpawn, new Vector3 (i, j, 0), Quaternion.identity);
				clone.transform.parent = transform;
			}
		}

	}



	void Movement ()
	{
		enemigoPadre.position += Vector3.right * Espeed;
		foreach (Transform Enemy in enemigoPadre) {
			if (Enemy.position.x < -6 || Enemy.position.x > 6) {
				Espeed = -Espeed;
				enemigoPadre.position += Vector3.down * 0.5f;
				return;
			}
		
			//EnemyBullet
			if (Random.value > fireRate) 
			{
				print (fireRate);
				Instantiate (Shot, Enemy.position, Enemy.rotation);
			}


			if (Enemy.position.y <= -3.5f) {
				
				GameOver.isPlayerDead = true;
				Time.timeScale = 0;
			}
		}
		if (enemigoPadre.childCount == 1) {
			CancelInvoke ();
			InvokeRepeating ("Movement", 0.1f, 0.3f);
		
		}
		if (enemigoPadre.childCount == 0) {
			winText.enabled = true;

		}
	}
}


