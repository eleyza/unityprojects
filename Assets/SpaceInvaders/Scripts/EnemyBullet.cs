﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {
	private Transform bullet;
	public float beSpeed;
	// Use this for initialization
	void Start () {
		bullet = GetComponent <Transform> ();
		
	}
	void FixedUpdate(){
		bullet.position += Vector3.up * -beSpeed;
		if (bullet.position.y <= -10)
			Destroy (bullet.gameObject);
	}
	// Update is called once per frame
	//void OnTriggerEnter (Collider2D other)
	//{
		//if (other.tag == "Player") {
			//Destroy (other.gameObject);	
			//Destroy (gameObject);
		//GameOver.isPlayerDead = true;
		//} else if (other.tag == "defense")
		//{
			//GameObject playerDefense = other.gameObject;
			//DefenseHealth baseHealth = playerDefense.GetComponent<DefenseHealth> ();
			//baseHealth.health -= 1;
			//Destroy (gameObject);
		//}

	//}
}