﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour {
	private Transform player;
	public float speed;
	public float MaxBound, MinBound;


	public float waitBetShots;
	private float nextShot;
	public GameObject shot;
	public Transform shotspawn;
	 

	// Use this for initialization
	void Start () {
		player = GetComponent<Transform> ();

	}
	
	// Update is called once per frame
	void Update () {
		//player movement

		float h = Input.GetAxis ("Horizontal");
		if (player.position.x < MinBound && h < 0)
			h = 0;
		else if (player.position.x > MaxBound && h > 0)
			h = 0;
		player.position += Vector3.right * h * speed;

		//bullet trigger
		 
		if (Input.GetKey (KeyCode.Space) || Input.GetButtonDown ("Fire1") && Time.time > nextShot) //condicion de presionar un boton, || significa "ò?"
		
		{
			nextShot = Time.time + waitBetShots;  
			Instantiate (shot, shotspawn.position, Quaternion.identity);// aparece la bala 

		}
	}
	


}
