﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Resset : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.R)) {
			Score.PlayerScore = 0;
			GameOver.isPlayerDead = false;
			Time.timeScale = 1;
			SceneManager.LoadScene ("spaceInvaders");
		}
		
	}
}
