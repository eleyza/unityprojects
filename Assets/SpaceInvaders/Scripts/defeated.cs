﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class defeated : MonoBehaviour {
	private Transform playerDefense;
	// Use this for initialization
	void Start () {
		playerDefense = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (playerDefense.childCount == 0)
			GameOver.isPlayerDead = true;
	}
}
