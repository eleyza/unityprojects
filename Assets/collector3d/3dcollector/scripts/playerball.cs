﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerball : MonoBehaviour {
	

	public float speed = 5;
	private Rigidbody rb;
	private int count;
	public Text score;
	public GameObject victorytext;


	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		count = 0;
	}
	void FixedUpdate ()
	{
		if (!plataform.dead) {
			float h = Input.GetAxis ("Horizontal");
			float v = Input.GetAxis ("Vertical");

			Vector3 movement = new Vector3 (h, 0.0f, v);

			rb.AddForce (movement * speed);
		}
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("pickup")) {
			other.gameObject.SetActive (false);
			count++;
			score.text = "Score: " + count.ToString ();
			if (count == 10)
				victorytext.SetActive (true);
				
		}
	}

}

