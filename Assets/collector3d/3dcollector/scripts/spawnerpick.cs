﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnerpick : MonoBehaviour {


	public GameObject pickups;


	// Use this for initialization
	void Start () {
		
			for (int i = 0; i < 10; i++) {
				float rndX = Random.Range (-16f, 16f);
				float rndZ = Random.Range (-16f, 16f);
				Instantiate (pickups, new Vector3(rndX,0,rndZ), Quaternion.identity);	
			}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
