﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cubo : MonoBehaviour {

	// Use this for initialization
	public Rigidbody rb;
	private int valor;
	bool jump;
	public int impulso;
	private bool isGrounded;
	public GameObject cube;
	public float speed; 
	float rndy = 3;
	float rndz = 3;
		
	    void Start () {
		
		rb = GetComponent<Rigidbody>();
		valor = 0;

	}

	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.Space)) 
		{
			valor += impulso;
		}
	
		if (Input.GetKeyUp (KeyCode.Space)) 
		{
			jump = true;
			spawn (); 
			StartCoroutine("stopJump");

		} 
		if (Input.GetKey (KeyCode.W)) 
		{
			transform.Translate (Vector3.forward * Time.deltaTime * speed);
		}


	}

	void FixedUpdate() {
		if ( jump )
		{
			rb.AddForce(0,valor, 20);	
			                   
		}
	}

	IEnumerator stopJump ()
	{
		yield return new WaitForSeconds(.3f);
		jump = false;

	} 


	void OnCollisionStay(Collision other){
		isGrounded = true;


	}
	void OnCollisionExit(Collision other){
		if(isGrounded){
			isGrounded = false; 
			Destroy(other.gameObject);
		}
	}


	void spawn ()
	{
		
		Instantiate (cube, new Vector3 (0,rndy,rndz),Quaternion.identity);
		rndy= rndy+3;
		rndz= rndz+3;

	}
}