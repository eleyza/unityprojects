﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour {

	public int speed = 10;
	private bool dodge;
	private float directx = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.LeftArrow)) 
			transform.Translate (Vector3.left * Time.deltaTime * speed);
		StartCoroutine ("line(false)");

		if (Input.GetKeyDown(KeyCode.RightArrow))
			transform.Translate (Vector3.right * Time.deltaTime * speed);
		StartCoroutine ("line(true)");
	
}
	IEnumerator line(bool right)
	{
		if (!dodge)
			dodge = true;
		directx = transform.position.x;
		if (!right)
			while (transform.position.x > directx - 1)
				transform.Translate (Vector3.left * Time.deltaTime * speed);
		else
			while (transform.position.x < directx + 1)
				transform.Translate (Vector3.right * Time.deltaTime * speed);
		dodge = false;
		yield return new WaitForEndOfFrame ();

	}
}