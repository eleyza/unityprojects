﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class backgroundScroll : MonoBehaviour {
	Renderer rend;

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer> ();
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!raceGameManager.GM.Gameover) {
			float offset = Time.time * raceGameManager.GM.scrollspeed;
			rend.material.SetTextureOffset ("_MainTex", new Vector2 (0, offset));
		}
	}
}
