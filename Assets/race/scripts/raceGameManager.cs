﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class raceGameManager : MonoBehaviour {
	public static raceGameManager GM;
	public Text Gameovertext;
	public Text Scoretext;
	public bool Gameover = false;
	public GameObject restartButton;

	private int score = 0;
	public float scrollspeed = 0.5f;

	void Awake()
	{
		if (GM == null)
		{
			GM = this;
			//DontDestroyOnLoad(this);
		}
		else
		{
			Destroy(gameObject);
		}
	}
	// Use this for initialization
	void Start () {
		Gameover = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}
	public void Score(){

		if (Gameover) 
		{
			return;
		}

		score++;
		Scoretext.text = "Score: " + score.ToString ();

	}
	public void cardDied()
	{
		
		Gameovertext.enabled = true;
		Gameover = true;
		restartButton.SetActive(true);
	}

}
